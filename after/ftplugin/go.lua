local g = vim.g
local opt = vim.opt

--- Tabulation ---
opt.expandtab = false
opt.smarttab = true
opt.tabstop = 4

-- For gopls linting without go.mod file. May be should be removed. --
-- The path for the gopls should be equal GOPATH.
-- g.ale_lsp_root = {
--     gopls = os.getenv('HOME') .. '/go',
-- }
