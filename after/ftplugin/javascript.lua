local opt = vim.opt

--- Tabulation ---
opt.shiftwidth = 2
opt.tabstop = 2
