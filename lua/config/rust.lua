local g = vim.g

return {
  setup = function()
    g.rustfmt_autosave = 1
    g.rustfmt_emit_files = 1
    g.rustfmt_fail_silently = 0
  end,
}
