return {
  backends = { "treesitter" },
  on_attach = function(bufnr)
    vim.keymap.set('n', 'K', '<cmd>AerialPrev<CR>', { buffer = bufnr })
    vim.keymap.set('n', 'J', '<cmd>AerialNext<CR>', { buffer = bufnr })
    vim.keymap.set('n', '<C-o>', '<cmd>AerialToggle!<CR>', { buffer = bufnr })
  end,
}
