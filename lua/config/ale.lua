local g = vim.g

return {
  setup = function()
    g.ale_linters = {
      python = {
        'flake8',
        'pyflakes',
        'pycodestyle',
      },
      go = {
        'gopls',
        'gofmt',
        'golint',
        'govet',
        'gobuild',
      },

      rust = 'all',
    }
    g.ale_linters_ignore = {
      python = {
        'jedils',
      },
    }

    g.ale_fixers = {
      go = {
        'goimports',
        'gofmt',
      }
    }
    g.ale_fix_on_save = 1
    g.ale_echo_msg_format = '[%linter%] [%severity%] %code: %%s'
    g.ale_python_flake8_options = '--config=.flake8'
    g.ale_history_log_output = 1
    g.ale_use_neovim_diagnostics_api = 0

    vim.diagnostic.disable()
  end,
}
