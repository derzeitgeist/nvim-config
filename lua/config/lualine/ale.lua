local required, lualine_require = pcall(require, 'lualine_require')
if not required then
  return nil
end

local modules = lualine_require.lazy_require {
  sources = 'lualine.components.diagnostics.sources',
  utils = 'lualine.utils.utils',
}

local sections_to_problem_types = {
  error = {
    'error',
    'style_error',
  },
  warn  = {
    'warning',
    'style_warning',
  },
}

local component = lualine_require.require('lualine.components.diagnostics'):extend()

local function get_ale_lines(buffer)
  local result = {}
  for section, problem_types in pairs(sections_to_problem_types) do
    for _, problem_type in ipairs(problem_types) do
      local ok, data = pcall(vim.fn['ale#statusline#FirstProblem'], buffer, problem_type)
      if ok and next(data) ~= nil then
        result[section] = data.lnum
        break
      end
    end
  end
  return result
end

local function get_ale_counts(buffer)
  local result = {
    error = 0,
    warn = 0,
    info = 0,
    hint = 0,
  }
  local ok, data = pcall(vim.fn['ale#statusline#Count'], buffer)
  if ok then
    result.error = data.error + data.style_error
    result.warn = data.warning + data.style_warning
    result.info = data.info
  end
  return result
end

function component:init(options)
  component.super.init(self, options)
  self.options.label = self.options.label or 'ALE:'
  self.last_diagnostics_lines = {}
end

function component:update_status()
  local bufnr = vim.api.nvim_get_current_buf()

  local diagnostics_count
  local diagnostics_lines

  local result = { self.options.label }

  if self.options.update_in_insert or vim.api.nvim_get_mode().mode:sub(1, 1) ~= 'i' then
    diagnostics_count = get_ale_counts(bufnr)
    diagnostics_lines = get_ale_lines(bufnr)

    -- Save ALE counts and line numbers for insert mode
    self.last_diagnostics_count[bufnr] = diagnostics_count
    self.last_diagnostics_lines[bufnr] = diagnostics_lines
  else
    -- Use cached line numbers and counts in insert mode with update_in_insert disabled
    diagnostics_count = self.last_diagnostics_count[bufnr] or { error = 0, warn = 0, info = 0, hint = 0 }
    diagnostics_lines = self.last_diagnostics_lines[bufnr] or {}
  end

  local always_visible = false
  if type(self.options.always_visible) == 'boolean' then
    always_visible = self.options.always_visible
  elseif type(self.options.always_visible) == 'function' then
    always_visible = self.options.always_visible()
  end

  -- Format the counts with symbols and highlights
  if self.options.colored then
    local colors, bgs = {}, {}
    for name, hl in pairs(self.highlight_groups) do
      colors[name] = self:format_hl(hl)
      bgs[name] = modules.utils.extract_highlight_colors(colors[name]:match('%%#(.-)#'), 'bg')
    end
    local previous_section, padding
    for _, section in ipairs(self.options.sections) do
      if diagnostics_count[section] ~= nil and (always_visible or diagnostics_count[section] > 0) then
        padding = previous_section and (bgs[previous_section] ~= bgs[section]) and ' ' or ''
        previous_section = section
        lnum = ''
        if diagnostics_lines[section] ~= nil then
            lnum = '(#' .. diagnostics_lines[section] .. ')'
        end
        table.insert(result, colors[section] .. padding .. self.symbols[section] .. diagnostics_count[section] .. lnum)
      end
    end
  else
    for _, section in ipairs(self.options.sections) do
      if diagnostics_count[section] ~= nil and (always_visible or diagnostics_count[section] > 0) then
        table.insert(result, self.symbols[section] .. diagnostics_count[section])
      end
    end
  end
  return table.concat(result, ' ')
end

return component
