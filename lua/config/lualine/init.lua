local ale = require('config.lualine.ale')

return {
  options = {
    theme = 'iceberg_dark',
    disabled_filetypes = {
      statusline = {
        'packer',
        'aerial',
        'NvimTree',
      }
    },
    section_separators = '',
    component_separators = '',
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {
      {
        'branch',
        icon = 'ᚠ',
      },
      {
        ale,
        sections = {
          'error',
          'warn',
        },
        symbols = {
          error = 'E',
          warn = 'W',
        },
        colored = true,
        always_visible = true,
        label = 'ᛟ',
      },
    },
    lualine_c = {
      {
        'filename',
        path = 1,
      }
    },
    lualine_x = {
      'encoding',
      {
        'fileformat',
        symbols = {
          unix = 'UNIX',
          dos = 'DOS',
          mac = 'MAC',
        }
      },
      'filetype',
    },
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  tabline = {
    lualine_a = {
      {
        'buffers',
        filetype_names = {
          NvimTree = 'FILESYSTEM',
        },
        symbols = {
          alternate_file = '',
        },
        component_name = 'buffers',
      },
    },
    lualine_b = {},
    lualine_c = {},
    lualine_x = {},
    lualine_y = {},
    lualine_z = {},
  }
}
