return {
    ale = require('config.ale'),
    lualine = require('config.lualine'),
    nvimtree = require('config.nvimtree'),
    aerial = require('config.aerial'),
    treesitter = require('config.treesitter'),
    rust = require('config.rust'),
    coc_nvim = require('config.coc_nvim')
}
