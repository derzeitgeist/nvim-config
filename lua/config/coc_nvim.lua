local key = vim.keymap.set

local options = {silent = true, nowait = true, expr = true}

return {
  setup = function()
      key("n", "<C-Down>", 'coc#float#has_scroll() ? coc#float#scroll(1) : "<C-f>"', options)
      key("n", "<C-Up>", 'coc#float#has_scroll() ? coc#float#scroll(0) : "<C-b>"', options)

      key("i", "<C-Down>", 'coc#float#has_scroll() ? "<c-r>=coc#float#scroll(1)<cr>" : "<Right>"', options)
      key("i", "<C-Up>", 'coc#float#has_scroll() ? "<c-r>=coc#float#scroll(0)<cr>" : "<Left>"', options)

      key("v", "<C-Down>", 'coc#float#has_scroll() ? coc#float#scroll(1) : "<C-f>"', options)
      key("v", "<C-Up>", 'coc#float#has_scroll() ? coc#float#scroll(0) : "<C-b>"', options)
  end,
}
