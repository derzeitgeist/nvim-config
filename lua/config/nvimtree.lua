return {
  hijack_cursor = true,
  filters = {
    dotfiles = false,
    custom = {
      '^__pycache__$',
      '^.*\\.pyc$',
      '^.*\\.o$',
      '^.*\\.a$',
      '^.*\\.sw.$',
    },
  },
  view = {
    centralize_selection = false,
    cursorline = false,
  },
  renderer = {
    icons = {
      web_devicons = {
        file = {
          enable = true,
          color = false,
        },
        folder = {
          enable = false,
          color = false,
        },
      },
      padding = " ",
      show = {
        file = true,
        folder = false,
        folder_arrow = true,
        git = true,
        modified = true,
        hidden = false,
        diagnostics = true,
        bookmarks = true,
      },
      glyphs = {
        default = "",
        symlink = "",
        bookmark = "󰆤",
        modified = "●",
        hidden = "󰜌",
        folder = {
          arrow_closed = "",
          arrow_open = "",
          default = "",
          open = "",
          empty = "",
          empty_open = "",
          symlink = "",
          symlink_open = "",
        },
        git = {
          unstaged = "✗",
          staged = "✓",
          unmerged = "",
          renamed = "➜",
          untracked = "★",
          deleted = "",
          ignored = "◌",
        },
      },
    },
  },
  git = {
      enable = false,
      ignore = false,
      timeout = 500,
  },
}
