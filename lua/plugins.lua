_G.config = require('config')
vim.cmd('packadd packer.nvim')


return require('packer').startup(function()
  -------------------
  -- Packer itself --
  -------------------
  use('wbthomason/packer.nvim')

  ------------
  -- Themes --
  ------------
  use('cocopon/iceberg.vim')

  --------
  -- UI --
  --------
  use{
    'kyazdani42/nvim-tree.lua',
    requires = 'kyazdani42/nvim-web-devicons',
    config = function()
      require('nvim-tree').setup(config.nvimtree)
    end
  }

  -- Statusline and buffersline --
  use {
    'nvim-lualine/lualine.nvim',
    requires = {
      'kyazdani42/nvim-web-devicons',
      opt = true
    },
    config = function()
      require('lualine').setup(config.lualine)
    end,
  }

  --------------------
  -- Autocompletion --
  --------------------
  use{
    'neoclide/coc.nvim',
    branch = 'release',
    config = function()
      config.coc_nvim.setup()
    end,
  }

  --------------------------------------
  -- Lexicon, punctuation and linting --
  --------------------------------------
  use('cohama/lexima.vim')
  -- Async linting engine --
  use{
    'dense-analysis/ale',
    config = function()
      config.ale.setup()
    end,
  }

  -- AST parser --
  use{
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate',
    config = function()
      require('nvim-treesitter.configs').setup(config.treesitter)
    end,
  }

  -- Classes and functions navigation, uses tree-sitter as its backend --
  use {
    'stevearc/aerial.nvim',
    config = function()
      require('aerial').setup(config.aerial)
    end,
  }

  -- Russian chars are available for cmd, navigation etc --
  use('powerman/vim-plugin-ruscmd')

  -- Autocomments --
  use{
    'numToStr/Comment.nvim',
    config = function()
      require('Comment').setup()
    end,
  }

  -- Simple tool for closing buffers --
  use('moll/vim-bbye')

  -----------------------
  -- Program languages --
  -----------------------
  use{
    'rust-lang/rust.vim',
    config = function()
      config.rust.setup()
    end,
  }

  -- Colored highlights for strings like '#fffffff' --
  use('ap/vim-css-color')

  -- HTML tags autoclose --
  use('alvan/vim-closetag')

  ----------------------------------------
  -- NVim plugin development, debug etc --
  ----------------------------------------
  --[[ use{
    'AckslD/messages.nvim',
    config = function()
      require("messages").setup()
    end,
  } ]]
  -- require("packer").sync()
end)
