local opt = vim.opt
local cmd = vim.cmd
local g = vim.g

--- Basics ---
opt.encoding = 'utf-8'
opt.mouse = 'c'

g.loaded_netrw = 1
g.loaded_netrwPlugin = 1

--- Colorcolumn ---
opt.colorcolumn = {80}

--- Colors and other basic settings ---
opt.syntax = 'on'
opt.background = 'dark'

opt.ruler = true
opt.hidden = true
opt.number = true
opt.showcmd = false

--- Tabulation ---
opt.expandtab = true
opt.smarttab = true
opt.autoindent = true
opt.laststatus = 2
opt.shiftwidth = 4
opt.tabstop = 4

--- Search ---
opt.ignorecase = true
opt.smartcase = true
opt.showmatch = true
opt.hlsearch = true

--- Colorscheme and font ---
opt.termguicolors = true
opt.fillchars = 'vert:█'
cmd.colorscheme('iceberg')

cmd.hi('WinSeparator guifg=#0f1117')
