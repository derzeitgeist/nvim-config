local map = vim.keymap.set
local defaults = {noremap = true, silent = true}

--- NvimTree ---
map('n', '<C-t>', ':NvimTreeToggle<CR>', defaults)  

--- Moving among buffers and removing buffers with Ctrl ---
map('n', '<C-k>', ':bnext<CR>', defaults)  
map('n', '<C-j>', ':bprev<CR>', defaults)  
map('n', '<C-d>', ':Bdelete!<CR>', defaults)  

map('i', '<silent>', '<expr><TAB>',  defaults)
