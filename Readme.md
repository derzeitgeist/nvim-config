# Neovim config for python development

Install packages for coc-nvim for arch:
```
sudo pacman -S nodejs npm python-jedi
```

Install packer.nvim:

```
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

Inside nvim:

```
:PackerSync
:PackerCompile
:PackerInstall

:CocInstall coc-jedi
```
